# Guided Tenable Agent Installer

Installs the Tenable Agent on the current system, and links to an Intruder account.

# Description

Installs the Tenable Agent on the current system. The agent is linked to an Intruder account using the provided NessusKey and CustomerUUID values. The target within your Intruder account will use the current system's hostname.
    
If you provide a path to an MSI that file will be used to install the agent, with the correct configuration. If no MSI file path is provided the script will download the latest Windows agent. If the current system is an x64 system the x64 agent will be installed, otherwise the Win32 agent will be installed.

You MUST accept Tenable's License Agreement if you intend to download, install and use the Tenable Agent. The License agreement can be found here at https://cloud.tenable.com/print-eula.html. Once you have read and are willing to accept the license agreement you MUST provide the -AcceptEULA flag when executing this script.

This script will auto-detect when it is not run with Administrator privileges, and it will auto-elevate asking for permission when UAC is configured to do so.

# Examples

Double-click the `.ps1` file to be guided through the installation process.

Download the agent directly from Tenable and install the agent on the current machine:
```
PS> .\install-agent.ps1 -AcceptEULA -NessusKey "a026bd7c8e41ff2b1bf05c497310ff5947f2aac7799dee0d6d7b89cf89ceacc7" -CustomerUUID "28e5b458-41c7-45f3-83ec-0097749ef447"
```

Install the agent using an already downloaded Nessus Agent `.msi` file: 
```
PS> .\install-agent.ps1 -AcceptEULA -MsiPath "\\FILESERVER\software\NessusAgent-10.1.3-x64.msi" -NessusKey "a026bd7c8e41ff2b1bf05c497310ff5947f2aac7799dee0d6d7b89cf89ceacc7" -CustomerUUID "28e5b458-41c7-45f3-83ec-0097749ef447"
```
