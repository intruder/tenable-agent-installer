<#
.SYNOPSIS
    Installs the Tenable Agent on the current system.
.DESCRIPTION
    Installs the Tenable Agent on the current system. The agent is linked to an Intruder account using the provided NessusKey and CustomerUUID values.
    The target within your Intruder account will use the current system's hostname.
    
    If you provide a path to an MSI that file will be used to install the agent, with the correct configuration. If no MSI file path is provided the script will download the latest Windows agent.
    If the current system is an x64 system the x64 agent will be installed, otherwise the Win32 agent will be installed.

    You MUST accept Tenable's License Agreement if you intend to download, install and use the Tenable Agent. The License agreement can be found here at https://cloud.tenable.com/print-eula.html
    Once you have read and are willing to accept the license agreement you MUST provide the -AcceptEULA flag when executing this script.

    This script MUST be run as an administrator.
.PARAMETER NessusKey
    The NessusKey that was provided by Intruder. This is a 64 random character text string.
.PARAMETER CustomerUUID
    Your Intruder customer unique identifier. UUIDs have the following format: 3907d597-cdfa-4cf4-82ba-0c2f1b7651f9.
.PARAMETER MsiPath
    The path to your pre-downloaded Nessus Agent installer (.msi) file.
.PARAMETER AcceptEULA
    You must provide this flag if you accept Tenable's license agreement, which can be found here: https://cloud.tenable.com/print-eula.html
.INPUTS
    None. You cannot pipe objects to this script.
.OUTPUTS
    None. This script does not generate any output.
.EXAMPLE
    PS> .\install-agent.ps1 -AcceptEULA -NessusKey "a026bd7c8e41ff2b1bf05c497310ff5947f2aac7799dee0d6d7b89cf89ceacc7" -CustomerUUID "28e5b458-41c7-45f3-83ec-0097749ef447"
.EXAMPLE
    PS> .\install-agent.ps1 -AcceptEULA -MsiPath "\\FILESERVER\software\NessusAgent-10.1.3-x64.msi" -NessusKey "a026bd7c8e41ff2b1bf05c497310ff5947f2aac7799dee0d6d7b89cf89ceacc7" -CustomerUUID "28e5b458-41c7-45f3-83ec-0097749ef447"
#>
param (
    [Parameter(Mandatory)]
    [string]$NessusKey,

    [Parameter(Mandatory)]
    [string]$CustomerUUID,

    [string]$MsiPath = "",
    
    [switch]$AcceptEULA = $false
)

# Download config
$AGENT_DETAILS_ENDPOINT = "https://www.tenable.com/downloads/api/v1/public/pages/nessus-agents"
$AGENT_DOWNLOAD_URL_FROMAT_STR = "https://www.tenable.com/downloads/api/v1/public/pages/nessus-agents/downloads/{0}/download?i_agree_to_tenable_license_agreement=true"
$TENABLE_EULA_URL = "https://cloud.tenable.com/print-eula.html"
# Static install config
$NESSUS_SERVER = "cloud.tenable.com:443"
$WORKING_DIR = "C:\Temp"
$LOG_PATH = "C:\Temp\agent_install_log.log"
# Customer link config
$NESSUS_KEY = "{{ NESSUS_KEY }}"
$CUST_UUID = "{{ CUST_UUID }}"

#
# Check we're running as an Administrator
#
function Get-IsAdministrator {
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    return $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
}

#
# Function to fetch Nessus Agent Information
#
function Fetch-AgentInformation {

    param (
        [string]
        $Endpoint
    )

    $response = Invoke-RestMethod -Uri "$Endpoint"
    
    return $response
}

#
# Get array of all available agents (sorted by version from newest to oldest)
#
function Get-AvailableAgentVersions {

    param (
        [PSObject]
        $AgentInformation
    )

    $versions = @()
    foreach ($item in $AgentInformation.Products.PSObject.Properties) {
        if ($item.Name.Contains("nessus-agents")) {
            $version = [regex]::match($item.Name,"(\d+\.\d+\.\d+)").Groups[1].Value
            $versions += $version
        }
    }

    return $versions | sort {[version] $_} -Descending
}

#
# Enumerate AgentInformation from Fetch-AgentInformation and get the latest version number
#
function Get-LatestAgentVersion {
    [OutputType([string])]

    param (
        [PSObject]
        $AgentInformation
    )

    $versions = Get-AvailableAgentVersions -AgentInformation $AgentInformation

    return $versions | sort {[version] $_} -Descending | Select-Object -First 1
}

#
# Download the specified agent, returns the file path where the agent was saved
#
function Download-Agent {
    [OutputType([string])]
    param (
        [Parameter(Mandatory)]
        [PSObject]$AgentInformation,

        [Parameter(Mandatory)]
        [ValidateSet("Win32", "x64")]
        [string]$Arch,

        [Parameter(Mandatory)]
        [ValidatePattern("\d+\.\d+\.\d+")]
        [string]$Version,

        [switch]$AcceptEULA
    )

    if (!$AcceptEULA) {
        Write-Error "You have not accepted Tenable's EULA. You must accept the Tenable EULA before continuing. The EULA can be found here: $TENABLE_EULA_URL"
        throw "You have not accepted Tenable's EULA"
    }

    $availableVersions = Get-AvailableAgentVersions -AgentInformation $AgentInformation
    if ($Version -notin $availableVersions) {
        Write-Error "$Version not an available version number: $($availableVersions -Join `",`")"
        throw "invalid version number provided"
    }

    $agentDetail = $AgentInformation.products | Select-Object -ExpandProperty "nessus-agents-$Version" | Select-Object -ExpandProperty "downloads" | Where-Object -Property name -Match "NessusAgent-$Version-$Arch.msi"
    
    $agentDetail
    if (!$agentDetail -Or !$agentDetail.id -Or !$agentDetail.file) {
        Write-Error "Failed to fetch agent details"
        throw "Failed to fetch agent details"
    }

    $msiOutputPath = [string]($agentDetail | Select-Object -ExpandProperty "file" | Split-Path -leaf)

    $downloadUrl = [string]::Format($AGENT_DOWNLOAD_URL_FROMAT_STR, $agentDetail.id)
    Write-Host "Downloading to $msiOutputPath"
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-WebRequest $downloadUrl -OutFile $msiOutputPath
        $ProgressPreference = 'Continue'
    } catch {
        $StatusCode = $_.Exception.Response.StatusCode.value__
        Write-Error "Received $StatusCode response"
        throw "Non-200 response received"
    }
    Write-Host "Downloaded $msiOutputPath OK"

    Write-Host "Validating $msiOutputPath"
    $expectedFileHash = $agentDetail.meta_data.sha256
    $actualFileHash = (Get-FileHash -Algorithm SHA256 $msiOutputPath).Hash
    if ($expectedFileHash -ne $actualFileHash) {
        Write-Error "Actual file hash $actualFileHash does not match expected file hash $expectedFileHash"
        throw "File integrity cannot be confirmed"
    }
    Write-Host "Validated $msiOutputPath, hash is OK"
    return $msiOutputPath
}

#
# Download the latest agent for this system, returns the download location
#
function Download-LatestAgent {
    [OutputType([string])]
    param(
        [switch]$AcceptEULA
    )
    
    if (!$AcceptEULA) {
        Write-Error "You have not accepted Tenable's EULA. You must accept the Tenable EULA before continuing. The EULA can be found here: $TENABLE_EULA_URL"
        throw "You have not accepted Tenable's EULA"
    }

    $allAgentInfo = Fetch-AgentInformation -Endpoint $AGENT_DETAILS_ENDPOINT

    if (!$allAgentInfo) {
        Write-Error "Failed to get agent information"
        throw "Failed to get agent information"
    }

    $latestVersion = Get-LatestAgentVersion -AgentInformation $allAgentInfo

    if (!$latestVersion) {
        Write-Error "Failed to extract latest agent version from agent information"
        throw "Failed to extract latest agent version from agent information"
    }

    $osArch = "Win32" 
    if ([System.Environment]::Is64BitOperatingSystem) {
        $osArch = "x64"
    }

    $installLocation = Download-Agent -AgentInformation $allAgentInfo -Arch $osArch -Version $latestVersion -AcceptEULA
    return [string]$installLocation
}

function Install-Agent {
    param(
        [Parameter(Mandatory)]
        [string]$MsiPath,

        [Parameter(Mandatory)]
        [string]$NessusKey,

        [Parameter(Mandatory)]
        [string]$CustomerUUID
    )

    $customerUuidIsValid = [guid]::TryParse($CustomerUUID, $([ref][guid]::Empty))
    if (!$customerUuidIsValid) {
        Write-Error "Invalid customer UUID provided"
        throw "invalid customer uuid"
    }

    $computerName = [System.Net.Dns]::GetHostName()
    Write-Host "Using target name $computerName"

    $MSIInstallArgs = @(
        "/i"
        "`"$MsiPath`""
        "NESSUS_SERVER=$NESSUS_SERVER"
        "NESSUS_KEY=$NessusKey"
        "NESSUS_NAME=$CustomerUUID`_$computerName"
        "/qn"
        "/l*v"
        "`"$LOG_PATH`""
    )

    Write-Host "Installing agent"
    #Write-Host "msiexec.exe $MSIInstallArgs"
    Start-Process "msiexec.exe" -ArgumentList $MSIInstallArgs -Wait -NoNewWindow
    Write-Host "Installation complete"
}

#
# Runtime
#

function Show-LicenseWarning {
    $title = "Tenable License Agreement"
    $question = "You have not accepted Tenable's License Agreement. You must accept the Tenable License Agreement before continuing, it can be found here: https://cloud.tenable.com/print-eula.html. Do you accept Tenable's License Agreement?"
    $choices = @(
        [System.Management.Automation.Host.ChoiceDescription]::new("&Yes", "I accept the License Agreement")
        [System.Management.Automation.Host.ChoiceDescription]::new("&No", "I do NOT accept the License Agreement and will not continue")
    )
    $acceptedByUser = $Host.UI.PromptForChoice($title, $question, $choices, 1)
    if ($acceptedByUser -ne 0) {
        Write-Error "You have chosen to not accept Tenable's License Agreement. Exiting."
        Exit
    }
}

$isAdmin = Get-IsAdministrator
if (!$isAdmin) {
    Write-Host "Not running as administrator - relaunching"
    $argsString = ""
    ForEach ($Parameter in $MyInvocation.BoundParameters.GetEnumerator()) {
        $paramKey = $Parameter.Key
        $paramVal = $Parameter.Value
        if ($Parameter.Value.GetType().Name -Eq "SwitchParameter") {
            $argsString += " -$paramKey"
        } else {
            $argsString += " -$paramKey $paramVal"
        }
    }
    $ThisScriptPath = $MyInvocation.MyCommand.Path
    $CommandLine = "-File `"$ThisScriptPath`"" + $argsString
    Write-Host $CommandLine
    Start-Process -FilePath PowerShell.exe -Verb RunAs -ArgumentList $CommandLine
    Exit
}

if (!$AcceptEULA) {
    Show-LicenseWarning
}

# Set working directory (could be a temporary dir using GUIDs)
$workingLocationExists = [System.IO.Directory]::Exists($WORKING_DIR)
if (!$workingLocationExists) {
    Write-Host "Creating working directory $WORKING_DIR"
    New-Item -ItemType Directory -Path $WORKING_DIR
}
Set-Location -Path $WORKING_DIR

$downloadLocation = $MsiPath
if ($MsiPath -eq "") {
    $downloadLocation = Download-LatestAgent -AcceptEULA
}
$downloadLocation = $downloadLocation.Trim()

$downloadLocationExists = Test-Path -Path $downloadLocation -PathType Leaf
if (!$downloadLocationExists) {
    Write-Error "MSI location does not exist, check that it is accessible: $downloadLocation"
    Exit
}

Install-Agent -MsiPath $downloadLocation -NessusKey $NessusKey -CustomerUUID $CustomerUUID